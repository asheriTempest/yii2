<?php
/**
 * Created by PhpStorm.
 * User: deurl
 * Date: 12.03.2019
 * Time: 23:58
 */

namespace app\controllers;

use app\models\Page;
use yii\web\Controller;


class PagesController extends Controller
{
    public function actionIndex()
    {
        $pages = Page::find()->all();

        return $this->render('index', [
            'title' => 'Page',
            'pages' => $pages
        ]);
    }

    public function actionView($id)
    {
        $page = Page::findOne($id);

        if(!$page) {
            $this->redirect('/');
        }

        return $this->render('view', ['page' => $page]);
    }
}