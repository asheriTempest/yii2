<?php
/**
 * Created by PhpStorm.
 * User: deurl
 * Date: 10.03.2019
 * Time: 16:39
 */

namespace app\controllers;

use app\models\Comment;
use app\models\Post;
use yii\web\Controller;
use Yii;

/**
 * Class PostsController
 * @package app\controllers
 *
 * @var Comment[] $comment
 */

class PostsController extends Controller
{
    public function actionIndex()
    {
        $posts = Post::find()->all();

        return $this->render('index', [
            'title' => 'Posts',
            'posts' => $posts
        ]);
    }
    public function actionView($id)
    {
        $post = Post::findOne($id);
        $comment = new Comment();
        $comment->postId = $post->id;

        return $this->render('view',
            [
                'post' => $post,
                'comment' => $comment
            ]);
    }

    public function actionCreate()
    {
        $post = new Post();
        if (Yii::$app->request->isPost) {
//            $post->setAttributes(Yii::$app->request->post());
//            $post->attributes = Yii::$app->request->post();
            $post->load(Yii::$app->request->post());

            if($post->save()) {
                Yii::$app->session->setFlash('message', 'Post created');
                $this->redirect('/posts');
            } else {
                Yii::$app->session->setFlash('message', 'Can\'t created post');
            }
        }
        return $this->render('create', ['post' => $post]);
    }

    public function actionEdit($id)
    {
        $post = Post::findOne($id);
        if (Yii::$app->request->isPost) {
            $post->load(Yii::$app->request->post());
            $post->setScenario(Post::SCENARIO_EDIT_ADMIN);

            if($post->save()) {
                Yii::$app->session->setFlash('message', 'Post created');
                $this->redirect('/posts');
            } else {
                Yii::$app->session->setFlash('message', 'Can\'t created post');
            }
        }
        return $this->render('edit', ['post' => $post]);
    }

    public function actionDelete($id)
    {
        $post = Post::findOne($id);
        if($post->delete()) {
            Yii::$app->session->setFlash('message', 'Post id:' . $id . ' success deleted');
            $this->redirect('/posts');
        }
    }

    public function actionAddComment()
    {
        if(Yii::$app->request->isPost) {
            $comment = new Comment();
            $comment->load(Yii::$app->request->post());

            if($comment->save()) {
                $this->redirect(['/posts/view', 'id' => $comment->postId]);
            } else {
                 $this->render('view', [
                    'post' => $comment->post,
                    'comment' => $comment
                ]);
            }
        } else {
            $this->redirect('/posts');

        }
    }
}