<?php
/**
 * Created by PhpStorm.
 * User: deurl
 * Date: 12.03.2019
 * Time: 23:58
 */

namespace app\controllers;

use app\models\Product;
use yii\web\Controller;


class ProductsController extends Controller
{
    public function actionIndex()
    {
        $products = Product::find()->all();

        return $this->render('index', [
            'title' => 'Product',
            'products' => $products
        ]);
    }

    public function actionView($id)
    {
        $product = Product::findOne($id);

        if(!$product) {
            $this->redirect('/');
        }

        return $this->render('view', ['product' => $product]);
    }
}