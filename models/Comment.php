<?php
/**
 * Created by PhpStorm.
 * User: deurl
 * Date: 17.03.2019
 * Time: 16:48
 */

namespace app\models;

/**
 * Class Comment
 * @property Post $post
 */

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


class Comment extends ActiveRecord
{
    public static function tableName()
    {
        return 'comments';
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['name', 'content'], 'string'],
            [['name', 'content', 'postId'], 'required'],
        ]);
    }

    //$comment->post
    public function getPost()
    {
        return $this->hasOne(Post::class, ['id' => 'postId']);
    }
}