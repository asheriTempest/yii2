<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Comment;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property string $title
 * @property string $shortDescription
 * @property string $content
 * @property Comment[] $comments
 */
class Post extends \yii\db\ActiveRecord
{
    const SCENARIO_EDIT_ADMIN = 'edit_admin';
    const SCENARIO_EDIT_USER = 'edit_user';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'title', 'shortDescription'], 'safe'],

            [['content', 'title', 'shortDescription'],
                'required',
                'on' => [self::SCENARIO_EDIT_USER]
            ],
            [
                ['title'],
                'required',
                'on' => [self::SCENARIO_EDIT_ADMIN]
            ],
            [['content'], 'string'],
            [['title'], 'string', 'max' => 200],
            [['shortDescription'], 'string', 'max' => 256],
            ['title', 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'shortDescription' => 'Short Description',
            'content' => 'Content',
        ];
    }

    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_EDIT_ADMIN => [
                'title', 'shortDescription', 'content'
            ],
            self::SCENARIO_EDIT_USER => [
                'shortDescription', 'content'
            ]
        ]);
    }

    public function getComments()
    {
        return $this->hasMany(Comment::class, ['postId' => 'id']);
    }
}
