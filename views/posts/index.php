<?php
/**
 * @var \app\models\Post[] $posts
 * @var string $title
 */

$this->params['breadcrumbs'][] = $title;
?>
<?php if (\Yii::$app->session->hasFlash('message')) : ?>
    <?=\Yii::$app->session->getFlash('message')?>
<?php endif ?>
<h1><?=$title?></h1>

<?=\yii\helpers\Html::a(
    'Create Post',
    '/posts/create',
    ['class' => 'btn btn-success']
)?>

<table class="table">
    <thead>
        <th>ID</th>
        <th>Title</th>
        <th>Short Description</th>
        <th></th>
    </thead>
    <tbody>
    <?php foreach ($posts as $post) : ?>
        <tr>
            <td>
                <?=$post->id;?>
            </td>
            <td>
                <?=$post->title;?>
            </td>
            <td>
                <?=$post->shortDescription?>
            </td>
            <td>
<!--                <a href="/posts/view?id=--><?//=$post->id;?><!--"></a>-->
                <?=\yii\helpers\Html::a(
                    'Show more',
                    ['/posts/view', 'id' => $post->id],
                    ['class' => 'btn btn-success']
                )?>
                <?=\yii\helpers\Html::a(
                    'Edit',
                    ['/posts/edit', 'id' => $post->id],
                    ['class' => 'btn btn-primary']
                )?>
                <?=\yii\helpers\Html::a(
                    'Delete',
                    ['/posts/delete', 'id' => $post->id],
                    [
                        'class' => 'btn btn-danger',
                        'onclick' => 'return confirm(\'You are exactly walking to delete this entry?\')'
                    ]
                )?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

