<?php
/**
 * @var \app\models\Post $post
 */
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['/posts']];
$this->params['breadcrumbs'][] = ['label' => $post->title, 'url' => ['/posts/' . $post->id]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<?php if (\Yii::$app->session->hasFlash('message')) : ?>
    <?=\Yii::$app->session->getFlash('message')?>
<?php endif ?>
<?php if ($post->hasErrors()) : ?>
    <pre>
        <?=print_r($post->errors);?>
    </pre>
<?php endif; ?>
    <h1>Edit post #<?=$post->id?></h1>
<?php $form = \yii\widgets\ActiveForm::begin() ?>
<?= $form->field($post, 'title')?>
<?= $form->field($post, 'shortDescription')?>
<?= $form->field($post, 'content')->textarea(['rows' => 6])->label('Post content')?>
<?= \yii\helpers\Html::submitButton('Edit post', ['class' => 'btn btn-primary']) ?>
<?php \yii\widgets\ActiveForm::end() ?>