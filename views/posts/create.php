<?php
/**
 * @var \app\models\Post $post
 */
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['/posts']];
$this->params['breadcrumbs'][] = 'Create';

?>
<?php if (\Yii::$app->session->hasFlash('message')) : ?>
        <?=\Yii::$app->session->getFlash('message')?>
<?php endif ?>
<h1>Create new post</h1>
<?php if ($post->hasErrors()) : ?>
    <pre>
        <?=print_r($post->errors);?>
    </pre>
<?php endif; ?>

<?php $form = \yii\widgets\ActiveForm::begin() ?>
    <?= $form->field($post, 'title')?>
    <?= $form->field($post, 'shortDescription')?>
    <?= $form->field($post, 'content')->textarea(['rows' => 6])->label('Post content')?>
    <?= \yii\helpers\Html::submitButton('Create post', ['class' => 'btn btn-primary']) ?>
<?php \yii\widgets\ActiveForm::end() ?>