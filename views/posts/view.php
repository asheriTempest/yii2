<?php
/**
 * Created by PhpStorm.
 * User: deurl
 * Date: 10.03.2019
 * Time: 17:04
 *
 * @var \app\models\Post $post
 */
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['/posts']];
$this->params['breadcrumbs'][] = $post->title;
?>
<style>
    .actions {
        width: 100%;
        display: inline-flex;
    }
    .edit {
        padding-left: 10px;
    }
</style>
<h1><?=$post->title?></h1>
<p>
    <?=$post->content?>
</p>
<div class="actions">
    <div class="create">
        <?=\yii\helpers\Html::a(
            'Back to list',
            '/posts',
            ['class' => 'btn btn-success']
        )?>
    </div>
</div>
<hr>
<div class="comments">
    <h2>Comments</h2>
    <?php foreach ($post->getComments()->limit(5)->all() as $postComment) : ?>
        <div>
            Name: <?=$postComment->name?>
            <br>
            Comment: <?=$postComment->content?>
        </div>
    <?php endforeach; ?>
</div>
<hr>
<?php $form = \yii\widgets\ActiveForm::begin(['action' => '/posts/add-comment'])?>
    <?=$form->field($comment, 'postId')->hiddenInput()->label(false)?>
    <?=$form->field($comment, 'name')?>
    <?=$form->field($comment, 'content')->textarea(['rows' => 5])?>
    <button class="btn btn-success" type="submit">Add Comment</button>
<?php \yii\widgets\ActiveForm::end()?>
