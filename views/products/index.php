<?php
/**
 * @var \app\models\Product[] $products;
 * @var $title
 */
$this->params['breadcrumbs'][] = $title;
?>
<h1><?=$title?></h1>
<table class="table">
    <thead>
    <th>ID</th>
    <th>Title</th>
    <th>Price</th>
    <th></th>
    </thead>
    <tbody>
    <?php foreach ($products as $product) : ?>
        <tr>
            <td>
                <?=$product->id;?>
            </td>
            <td>
                <?=$product->title;?>
            </td>
            <td>
                <?=$product->price?>
            </td>
<!--            <td>--><?//=\yii\helpers\Html::a(
//                    'Show more...',
//                    '/products/view?id=' . $product->id
//                )?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
