<?php
/**
 * Created by PhpStorm.
 * User: deurl
 * Date: 10.03.2019
 * Time: 17:04
 *
 * @var \app\models\Product $product
 */
$this->params['breadcrumbs'][] = ['label' => 'Product', 'url' => ['/products']];
$this->params['breadcrumbs'][] = $product->title;
?>
<style>
    .actions {
        width: 100%;
        display: inline-flex;
    }
    .edit {
        padding-left: 10px;
    }
</style>
<h1><?=$product->title?></h1>
<p>
    Price: <?=$product->price?>
    <br>
    <?=$product->description?>
</p>
<div class="actions">
    <div class="create">
        <?=\yii\helpers\Html::a(
            'Back to list',
            '/products',
            ['class' => 'btn btn-success']
        )?>
    </div>
</div>
