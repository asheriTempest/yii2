<?php
/**
 * @var \app\models\Page[] $pages;
 * @var $title
 */
$this->params['breadcrumbs'][] = $title;
?>
<h1><?=$title?></h1>
<table class="table">
    <thead>
    <th>ID</th>
    <th>Title</th>
    <th>Intro</th>
    <th></th>
    </thead>
    <tbody>
    <?php foreach ($pages as $page) : ?>
        <tr>
            <td>
                <?=$page->id;?>
            </td>
            <td>
                <?=$page->title;?>
            </td>
            <td>
                <?=$page->intro?>
            </td>
            <td>
            <td><?=\yii\helpers\Html::a(
                    'Show more...',
                    '/pages/view?id=' . $page->id
                )?>
            </td>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
