<?php
/**
 * Created by PhpStorm.
 * User: deurl
 * Date: 10.03.2019
 * Time: 17:04
 *
 * @var \app\models\Page $page
 */
$this->params['breadcrumbs'][] = ['label' => 'Page', 'url' => ['/pages']];
$this->params['breadcrumbs'][] = $page->title;
?>
<style>
    .actions {
        width: 100%;
        display: inline-flex;
    }
    .edit {
        padding-left: 10px;
    }
</style>
<h1><?=$page->title?></h1>
<p>
    <?=$page->content?>
</p>
<div class="actions">
    <div class="create">
        <?=\yii\helpers\Html::a(
            'Back to list',
            '/pages',
            ['class' => 'btn btn-success']
        )?>
    </div>
</div>
