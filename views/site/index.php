<?php

/* @var $this yii\web\View
 *
 * @var \app\models\Product[] $products
 * @var \app\models\Orders[] $orders
 * @var \app\models\Page[] $pages
 *
 */

$this->title = 'My Yii Application';
?>
<style>
    .rows {
        width: 100%;
        display: inline-flex;
        /*justify-content: space-e;*/
    }
    .rows a {
        padding-left: 10px;
    }
</style>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-3">
                <h2>Pages</h2>
                <?php foreach ($pages as $page) : ?>
                    <div class="rows">
                       <div><?=$page->title?></div><a href="/pages/view?id=<?=$page->id?>">Details</a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="col-lg-3">
                <h2>Products</h2>
                <?php foreach ($products as $product) : ?>
                    <div class="rows">
                        <div><?=$product->title?></div><a href="/products/view?id=<?=$product->id?>">Details</a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="col-lg-6">
                <h2>Orders</h2>
                <?php foreach ($orders as $order) : ?>
                    <div class="col-md-6" style="padding-bottom: 15px;">
                        <div><?=$order->customer_name?></div>
                        <div><?=$order->email?></div>
                        <div><?=$order->phone?></div>
                        <div><?=$order->feedback?></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
</div>
