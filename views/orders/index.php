<?php
/**
 * @var \app\models\Orders[] $orders;
 * @var $title
 */
$this->params['breadcrumbs'][] = $title;
?>
<h1><?=$title?></h1>
<table class="table">
    <thead>
    <th>Customer name</th>
    <th>Email</th>
    <th>Phone</th>
    <th>Feedback</th>
    </thead>
    <tbody>
    <?php foreach ($orders as $order) : ?>
        <tr>
            <td>
                <?=$order->customer_name;?>
            </td>
            <td>
                <?=$order->email;?>
            </td>
            <td>
                <?=$order->phone?>
            </td>
            <td>
                <?=$order->feedback?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
