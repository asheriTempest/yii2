<?php

use yii\db\Migration;

/**
 * Class m190311_224729_addProducts
 */
class m190311_224729_addProducts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('products',[
            'title' => 'Apple',
            'alias' => 'apple',
            'price' => 500,
            'description' => 'IPhone'
        ]);

        $this->insert('products',[
            'title' => 'HTC',
            'alias' => 'htc',
            'price' => 300,
            'description' => 'HTC Phone'
        ]);

        $this->insert('products',[
            'title' => 'IPad',
            'alias' => 'ipad',
            'price' => 1000,
            'description' => 'Apple IPad'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190311_224729_addProducts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190311_224729_addProducts cannot be reverted.\n";

        return false;
    }
    */
}
