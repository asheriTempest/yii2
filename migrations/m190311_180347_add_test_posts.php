<?php

use yii\db\Migration;

/**
 * Class m190311_180347_add_test_posts
 */
class m190311_180347_add_test_posts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('posts',[
            'title' => 'Titan 1',
            'shortDescription' => 'Short description post 1',
            'content' => 'Content post 1'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190311_180347_add_test_posts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190311_180347_add_test_posts cannot be reverted.\n";

        return false;
    }
    */
}
