<?php

use yii\db\Migration;

/**
 * Class m190311_223805_createTableOrder
 */
class m190311_223805_createTableOrders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'customer_name' => $this->string()->notNull(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'feedback' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190311_223805_createTableOrders cannot be reverted.\n";

        return false;
    }
    */
}
