<?php

use yii\db\Migration;

/**
 * Class m190311_224439_addOrders
 */
class m190311_224439_addOrders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('orders',[
            'customer_name' => 'Robert',
            'email' => 'testmail@mail.none',
            'phone' => '+3800000000',
            'feedback' => 'Good feedback'
        ]);

        $this->insert('orders',[
            'customer_name' => 'Anna',
            'email' => 'testmail@mail.none',
            'phone' => '+3800000000',
            'feedback' => 'Good feedback 2'
        ]);

        $this->insert('orders',[
            'customer_name' => 'Julia',
            'email' => 'testmail@mail.none',
            'phone' => '+3800000000',
            'feedback' => 'Good feedback 3'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190311_224439_addOrders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190311_224439_addOrders cannot be reverted.\n";

        return false;
    }
    */
}
