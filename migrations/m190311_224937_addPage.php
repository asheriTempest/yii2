<?php

use yii\db\Migration;

/**
 * Class m190311_224937_addPage
 */
class m190311_224937_addPage extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('pages',[
            'title' => 'Test Page',
            'alias' => 'test-page',
            'intro' => 'Custom intro for page',
            'content' => 'Custom content for page'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190311_224937_addPage cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190311_224937_addPage cannot be reverted.\n";

        return false;
    }
    */
}
