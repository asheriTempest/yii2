<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m190317_142920_create_comments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('comments', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'content' => $this->text(),
            'postId' => $this->integer()
        ]);

        $this->addForeignKey(
            'fkCommetPost',
            'comments',
            'postId',
            'posts',
            'id',
            'CASCADE',
            'CASCADE'
            );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('comments');
    }
}
